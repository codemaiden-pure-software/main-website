using System.IO;
using Microsoft.Extensions.FileProviders;

namespace Microsoft.AspNetCore.Builder
{
	public static class ApplicationBuilderExtensions
	{
		public static IApplicationBuilder UseNodeModules(this IApplicationBuilder builder, string root)
		{
			var options = new StaticFileOptions
			{
				RequestPath = "/node_modules",
				FileProvider = new PhysicalFileProvider(Path.Combine(root, "node_modules"))
			};

			builder.UseStaticFiles(options);

			return builder;
		}
	}
}