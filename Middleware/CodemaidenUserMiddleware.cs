﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using codemaiden.info.Extensions;
using codemaiden.info.Models;
using codemaiden.info.Services;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Microsoft.AspNetCore.Builder
{
	public class CodemaidenUserMiddleware
	{
		private readonly RequestDelegate _next;

		public CodemaidenUserMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public Task Invoke(HttpContext context, IProfileStore profiles)
		{
			var userId = context.User.GetNameId();
			if (string.IsNullOrEmpty(userId)) return _next(context);

			var profile = profiles.GetProfileByAId(userId);
			if (profile == null)
			{
				var allowedRoutes = new[]
				{
					"/Account/Welcome",
					"/Account/Logout",
					"/Account/CreateProfile"
				};

				if (allowedRoutes.All(route => !route.Equals(context.Request.Path)))
				{
					context.Response.Redirect("/Account/Welcome");
					return Task.CompletedTask;
				}

				return _next(context);
			}

			context.Items.Add(UserProfile.SessionKey, profile);
			return _next(context);
		}
	}
}