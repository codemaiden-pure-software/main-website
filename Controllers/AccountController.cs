﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using codemaiden.info.Extensions;
using codemaiden.info.FormDataModels;
using codemaiden.info.Models;
using codemaiden.info.Services;
using codemaiden.info.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace codemaiden.info.Controllers
{
	public class AccountController : Controller
	{
		private IProfileStore _profileStore;

		public AccountController(IProfileStore profileStore)
		{
			_profileStore = profileStore;
		}

		[HttpGet]
		public IActionResult AccessDenied(string returnUrl)
		{
			return View("AccessDenied", returnUrl);
		}

		[HttpGet]
		public async Task Login(string returnUrl = "/")
		{
			await HttpContext.ChallengeAsync("Auth0", new AuthenticationProperties
			{
				RedirectUri = returnUrl
			});
		}

		// TODO make this POST
		[Authorize]
		[HttpGet]
		public async Task Logout()
		{
			await HttpContext.SignOutAsync("Auth0", new AuthenticationProperties
			{
				RedirectUri = Url.Action("Return")
			});
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
		}

		public IActionResult Return()
		{
			return RedirectToAction("Index", "Home");
		}

		[Authorize]
		public IActionResult Claims()
		{
			var claims = User.Claims.ToList();
			var id = User.GetNameId();
			return View(new UserProfileViewModel
			{
				Name = User.Identity.Name,
				EmailAddress = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value,
				ProfileImage = User.Claims.FirstOrDefault(c => c.Type == "picture")?.Value,
				CmUserProfile = HttpContext.Items[UserProfile.SessionKey] as UserProfile
			});
		}

		[Authorize(Policy = "IsSysOp")]
		public IActionResult PuddingKingdom()
		{
			return Content("Welcome to the pudding kingdom!!!");
		}

		[Authorize(Roles = "admin")]
		public IActionResult Admin()
		{
			return Content("Hello, admin!");
		}

		[Authorize]
		public IActionResult Welcome()
		{
			var errors = new List<string>();
			if (Request.Cookies.TryGetValue("FlashErrors", out var errorString))
			{
				errors.AddRange(errorString.Split(';').Select(error => error.FromBase64()));
				Response.Cookies.Delete("FlashErrors");
			}

			var vm = new AccountWelcomeViewModel
			{
				Errors = errors
			};
			return View(vm);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult CreateProfile(CreateProfileDataModel model)
		{
			if (!ModelState.IsValid)
			{
				var errors = new List<string>();
				foreach (var state in ModelState.Values)
				{
					foreach (var error in state.Errors) errors.Add(error.ErrorMessage.ToBase64());
				}

				Response.Cookies.Append("FlashErrors", errors.Aggregate((x, y) => $"{x};{y}"));
				return RedirectToAction("Welcome");
			}

			_profileStore.CreateProfile(new UserProfile
			{
				AuthId = User.GetNameId(),
				DisplayName = model.DisplayName,
				IsSysOp = false
			});
			return RedirectToAction("Claims");
		}
	}

	public class UserProfileViewModel
	{
		public string Name { get; set; }
		public string EmailAddress { get; set; }
		public string ProfileImage { get; set; }
		public UserProfile CmUserProfile { get; set; }
	}
}