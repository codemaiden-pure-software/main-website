﻿using codemaiden.info.Services;
using codemaiden.info.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace codemaiden.info.Controllers
{
	public class BlogController : Controller
	{
		private readonly IBlogData _blogPosts;

		public BlogController(IBlogData blogPosts)
		{
			_blogPosts = blogPosts;
		}

		public IActionResult Index()
		{
			return View(new BlogIndexViewModel(_blogPosts.AllBlogPosts()));
		}
	}
}