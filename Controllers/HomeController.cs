using Microsoft.AspNetCore.Mvc;

namespace codemaiden.info.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}

//		[HttpGet]
//		public IActionResult Index() {
//			var vm = new HomeIndexViewModel {
//				Courses = _courseData.FetchAll(),
//				CurrentMessage = _greeter.GetMessageOfTheDay()
//			};
//
//			return View(vm);
//		}
//
//		[HttpGet]
//		public IActionResult Details(int id) {
//			var course = _courseData.Fetch(id);
//			if (course == null) return NotFound();
//
//			return View(course);
//		}
//
//		[HttpGet]
//		[Authorize]
//		public IActionResult Create() {
//			return View();
//		}
//
//		[HttpPost]
//		[ValidateAntiForgeryToken]
//		[Authorize]
//		public IActionResult Create(CourseEditModel model) {
//			if (!ModelState.IsValid) return View();
//
//			if (model.Id != 0) return StatusCode(500);
//
//			var course = new Course {
//				Name = model.Name,
//				Subject = model.Subject
//			};
//
//			course = _courseData.Add(course);
//
//			return RedirectToAction(nameof(Details), new {id = course.Id});
//		}
//
//		[HttpGet]
//		[Authorize]
//		public IActionResult Edit(int id) {
//			var course = _courseData.Fetch(id);
//			if (course == null) return NotFound();
//
//			return View(new CourseEditModel(course));
//		}
//
//		[HttpPost]
//		[ValidateAntiForgeryToken]
//		[Authorize]
//		public IActionResult Edit(CourseEditModel model) {
//			if (!ModelState.IsValid) return View(model);
//
//			var course = _courseData.Fetch(model.Id);
//			if (course == null) return NotFound();
//
//			course.Name = model.Name;
//			course.Subject = model.Subject;
//
//			_courseData.Update(course);
//
//			return RedirectToAction(nameof(Details), new {id = course.Id});
//		}
	}
}