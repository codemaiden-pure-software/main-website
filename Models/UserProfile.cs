﻿using System.ComponentModel.DataAnnotations;

namespace codemaiden.info.Models
{
	public class UserProfile
	{
		public const string SessionKey = "UserProfile";

		[Key]
		public string AuthId { get; set; }

		public string DisplayName { get; set; }
		public bool IsSysOp { get; set; }
	}
}