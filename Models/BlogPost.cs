﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace codemaiden.info.Models
{
	public class BlogPost
	{
		[Key]
		public int BlogPostId { get; set; }

		public string Title { get; set; }

		[Column(TypeName = "ntext")]
		[MaxLength]
		public string Body { get; set; }
	}
}