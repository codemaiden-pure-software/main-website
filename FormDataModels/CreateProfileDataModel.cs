﻿using System.ComponentModel.DataAnnotations;
using codemaiden.info.Attributes;
using Microsoft.AspNetCore.Mvc;

namespace codemaiden.info.FormDataModels
{
	public class CreateProfileDataModel
	{
		[Required(ErrorMessage = "You must enter a display name.")]
		[MaxLength(24, ErrorMessage = "Your display name must be no more than 24 characters.")]
		public string DisplayName { get; set; }

		[Required]
		[MustBeTrue(ErrorMessage = "You must agree to the Terms and Conditions (or log out) to continue.")]
		public bool ToSAgreed { get; set; }
	}
}