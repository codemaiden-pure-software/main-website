﻿using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace codemaiden.info
{
	public class Program
	{
		public static void Main(string[] args)
		{
			BuildWebHost(args).Run();
		}

		public static IWebHost BuildWebHost(string[] args)
		{
			return WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.UseKestrel(options =>
				{
					options.Listen(IPAddress.Loopback, 25090,
						listenOptions => { listenOptions.UseHttps("Cert/testCert.pfx", string.Empty); });
				})
				.Build();
		}
	}
}