using Microsoft.Extensions.Configuration;

namespace codemaiden.info
{
	public class Greeter : IGreeter
	{
		private readonly string _greeting;

		public Greeter(IConfiguration config)
		{
			_greeting = config["Greeting"];
		}

		public string GetMessageOfTheDay()
		{
			return _greeting;
		}
	}
}