namespace codemaiden.info
{
	public interface IGreeter
	{
		string GetMessageOfTheDay();
	}
}