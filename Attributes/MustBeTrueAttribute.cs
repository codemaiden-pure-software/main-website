﻿using System.ComponentModel.DataAnnotations;

namespace codemaiden.info.Attributes
{
	public class MustBeTrueAttribute : ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			return value is bool b && b;
		}
	}
}