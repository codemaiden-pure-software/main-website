using Microsoft.AspNetCore.Mvc;

namespace codemaiden.info.ViewComponents
{
	public class GreeterViewComponent : ViewComponent
	{
		private readonly IGreeter _greeter;

		public GreeterViewComponent(IGreeter greeter)
		{
			_greeter = greeter;
		}

		public IViewComponentResult Invoke()
		{
			var model = _greeter.GetMessageOfTheDay();
			return View("Default", model);
		}
	}
}