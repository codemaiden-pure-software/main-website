﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using codemaiden.info.Data;
using codemaiden.info.Requirements;
using codemaiden.info.Requirements.Handlers;
using codemaiden.info.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace codemaiden.info
{
	public class Startup
	{
		private readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication(options =>
				{
					options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
					options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
					options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
					options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
				})
//				.AddOpenIdConnect(options => { _configuration.Bind("AzureAd", options); })
				.AddCookie()
				.AddOpenIdConnect("Auth0", options =>
				{
					options.Authority = $"https://{_configuration["Auth0:Domain"]}";
					options.ClientId = _configuration["Auth0:ClientId"];
					options.ClientSecret = _configuration["Auth0:ClientSecret"];
					options.ResponseType = "code";
					options.Scope.Clear();
					options.Scope.Add("openid");
					options.Scope.Add("profile");
					options.Scope.Add("email");

					options.TokenValidationParameters = new TokenValidationParameters
					{
						NameClaimType = "name",
						RoleClaimType = "https://codemaiden.info/roles"
					};

					options.CallbackPath = new PathString("/signin-auth0");
					options.ClaimsIssuer = "Auth0";
					options.Events = new OpenIdConnectEvents
					{
						OnRedirectToIdentityProviderForSignOut = context =>
						{
							var logoutUri =
								$"https://{_configuration["Auth0:Domain"]}/v2/logout?client_id={_configuration["Auth0:ClientId"]}";
							var postLogoutUri = context.Properties.RedirectUri;
							if (!string.IsNullOrEmpty(postLogoutUri))
							{
								if (postLogoutUri.StartsWith("/"))
								{
									var request = context.Request;
									postLogoutUri =
										$"{request.Scheme}://{request.Host}{request.PathBase}{postLogoutUri}";
								}

								logoutUri += $"&returnTo={Uri.EscapeDataString(postLogoutUri)}";
							}

							context.Response.Redirect(logoutUri);
							context.HandleResponse();
							return Task.CompletedTask;
						}
					};
				});

//			services.AddDbContext<IdentityDbContext>(options =>
//				options.UseSqlServer(_configuration.GetConnectionString("Codemaiden"),
//					optionsBuilder => optionsBuilder.MigrationsAssembly("codemaiden.info")));
//
//			services.AddIdentity<IdentityUser, IdentityRole>()
//				.AddEntityFrameworkStores<IdentityDbContext>()
//				.AddDefaultTokenProviders();

			services.AddSingleton<IGreeter, Greeter>();
			services.AddDbContext<CodemaidenDbContext>(options =>
				options.UseSqlServer(_configuration.GetConnectionString("Codemaiden")));
			services.AddScoped<IBlogData, SqlBlogData>();
			services.AddScoped<IProfileStore, SqlProfileStore>();
			services.AddMvc();
			services.AddScoped<IAuthorizationHandler, IsSysOpHandler>();
			services.AddAuthorization(options =>
			{
				options.AddPolicy("IsSysOp", policy => { policy.AddRequirements(new IsSysOpRequirement()); });
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IConfiguration config, IGreeter greeter,
			IProfileStore profiles)
		{
			if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

			app.UseStaticFiles();
			app.UseNodeModules(env.ContentRootPath);

			app.UseAuthentication();
			app.UseMiddleware<CodemaidenUserMiddleware>();

			app.UseStatusCodePages();

			app.Use((context, next) => {
				var claim = new Claim("pudding", "chan");
				context.User.AddIdentity(new ClaimsIdentity(new[] { claim }));
				return next();
			});
			app.UseMvc(ConfigureRoutes);
		}

		private void ConfigureRoutes(IRouteBuilder routeBuilder)
		{
			routeBuilder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
		}
	}
}