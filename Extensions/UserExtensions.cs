﻿using System.Linq;
using System.Security.Claims;
using codemaiden.info.Models;

namespace codemaiden.info.Extensions
{
	public static class UserExtensions
	{
		public static string GetNameId(this ClaimsPrincipal claimant)
		{
			return claimant.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
		}
	}
}