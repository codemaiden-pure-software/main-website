﻿using codemaiden.info.Models;
using Microsoft.AspNetCore.Http;

namespace codemaiden.info.Extensions
{
	public static class HttpContextExtensions
	{
		public static UserProfile GetProfile(this HttpContext context) => context.Items[UserProfile.SessionKey] as UserProfile;
		public static bool HasProfile(this HttpContext context) => context.GetProfile() != null;
	}
}