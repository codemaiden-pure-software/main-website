﻿using System;
using System.Text;

namespace codemaiden.info.Extensions
{
	public static class StringExtensions
	{
		public static string ToBase64(this string s) => Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
		public static string FromBase64(this string s) => Encoding.UTF8.GetString(Convert.FromBase64String(s));
	}
}