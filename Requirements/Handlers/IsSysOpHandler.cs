﻿using System.Threading.Tasks;
using codemaiden.info.Extensions;
using codemaiden.info.Models;
using codemaiden.info.Services;
using Microsoft.AspNetCore.Authorization;

namespace codemaiden.info.Requirements.Handlers
{
	public class IsSysOpHandler : AuthorizationHandler<IsSysOpRequirement>
	{
		private readonly IProfileStore ProfileStore;

		public IsSysOpHandler(IProfileStore profileStore)
		{
			ProfileStore = profileStore;
		}
		protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsSysOpRequirement requirement)
		{
			var profile = ProfileStore.GetProfileByAId(context.User.GetNameId());
			if (profile == null || !profile.IsSysOp)
				context.Fail();
			else
				context.Succeed(requirement);
			return Task.CompletedTask;
		}
	}
}