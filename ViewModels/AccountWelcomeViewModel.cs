﻿using System.Collections.Generic;

namespace codemaiden.info.ViewModels
{
	public class AccountWelcomeViewModel
	{
		public List<string> Errors { get; set; }
	}
}