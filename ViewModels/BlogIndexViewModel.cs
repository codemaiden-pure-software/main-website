﻿using System.Collections.Generic;
using codemaiden.info.Models;

namespace codemaiden.info.ViewModels
{
	public class BlogIndexViewModel
	{
		public BlogIndexViewModel(IEnumerable<BlogPost> blogPosts)
		{
			BlogPosts = blogPosts;
		}

		public IEnumerable<BlogPost> BlogPosts { get; set; }
	}
}