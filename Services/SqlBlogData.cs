﻿using System.Collections.Generic;
using codemaiden.info.Data;
using codemaiden.info.Models;

namespace codemaiden.info.Services
{
	public class SqlBlogData : IBlogData
	{
		private readonly CodemaidenDbContext _context;

		public SqlBlogData(CodemaidenDbContext context)
		{
			_context = context;
		}

		public IEnumerable<BlogPost> AllBlogPosts()
		{
			return _context.BlogPosts;
		}
	}
}