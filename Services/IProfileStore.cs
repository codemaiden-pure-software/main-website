﻿using codemaiden.info.Models;

namespace codemaiden.info.Services
{
	public interface IProfileStore
	{
		UserProfile GetProfileByAId(string aid);
		UserProfile CreateProfile(UserProfile profile);
	}
}