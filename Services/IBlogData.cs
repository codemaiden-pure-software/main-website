﻿using System.Collections.Generic;
using codemaiden.info.Models;

namespace codemaiden.info.Services
{
	public interface IBlogData
	{
		IEnumerable<BlogPost> AllBlogPosts();
	}
}