﻿using System.Linq;
using codemaiden.info.Data;
using codemaiden.info.Models;

namespace codemaiden.info.Services
{
	public class SqlProfileStore : IProfileStore
	{
		private readonly CodemaidenDbContext _context;

		public SqlProfileStore(CodemaidenDbContext context)
		{
			_context = context;
		}

		public UserProfile GetProfileByAId(string aid)
		{
			return _context.UserProfiles.SingleOrDefault(c => c.AuthId == aid);
		}

		public UserProfile CreateProfile(UserProfile profile)
		{
			_context.UserProfiles.Add(profile);
			_context.SaveChanges();
			return profile;
		}
	}
}