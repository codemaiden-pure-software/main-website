using codemaiden.info.Models;
using Microsoft.EntityFrameworkCore;

namespace codemaiden.info.Data
{
	public class CodemaidenDbContext : DbContext
	{
		public CodemaidenDbContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<BlogPost> BlogPosts { get; set; }
		public DbSet<UserProfile> UserProfiles { get; set; }
	}
}